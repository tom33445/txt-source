那一天，地球上的所有人們理解了這世上還有他們不了解的秘密存在的。

除了剛出生的嬰兒或是精神上已經死去的人外，其他人全都抬頭仰望著天空。
在空中出現了另一個星球。
那是跟地球很類似，有著藍色的海洋綠色的大地以及白色的雲朵的星球。
那個是異世界。

原本應該在宇宙中運行的衛星全都遭到破壊，各國的太空站也全部被消滅了，這讓各國陷入混亂當中，但各國各自觀察著這個世界，同時確認了上面有生命存在的痕跡，而且似乎能往來兩個不同的星球。
比較躁進的國家派了偵查機前往⋯然後確認到了那個世界是毫無疑問存在的。
行星出現的原因不明，說起來現在的狀態本身就是違反物理法則的，科學家們只能確認這一點而已。
彼此大氣相接的那顆行星上，幾乎找不到人工光源。
將之判斷為處女地的某國，為了佔領該處而派出有人飛機，但這個行動失敗了。
無人機能抵達的那個星球，其大氣圈卻意外的阻止了有人機的靠近，而那些飛機毫髮無傷的返回基地了。

所有人都在猜測著，那個究竟是什麼呢？
雖然各國的專家學者們都在進行研究，但在網路上面的一般民眾反而很普通的理解了那個世界的本質，
那就是異世界。
雖然完全不了解那個世界的事情，但卻深信著那就是異世界。
即使是個人能負擔起的望眼鏡，也能確認到那個星球上有的生命活動的痕跡，而且也能見到有智慧的生命體存在著。
全世界陷入了一股興奮當中。

然後過了三天，那個出現了。

在地球上超過兩千個地點的上空，出現了巨大的影像。
身穿著繡著金線的黑衣，戴著黃金面具，批著黑色的披肩，頭上戴著有角的頭盔，

「地球的各位，你們好」

那是個年輕男性的聲音，而且很不可思議的用著世界各地的語言與地球上的人們打招呼著。

「我是治理這個世界上領土最大的國家的魔王阿魯斯」

僅僅這麼一句話，就提供了非常多的情報。

「地球上的各位大概還不清楚現在到底發生什麼事情了吧，因此我現在就來為各位說明」

而這個的確是地球各國望眼欲穿的情報。

「首先是這個世界⋯以地球的話來說就類似宇宙吧，是有容量上限的。如果要問這個容量是什麼的話，解釋起來有點麻煩，簡單來說就是世界中所能存在的靈魂以及容量是有上限的，當容量快要到達極限的時候，就會像這樣讓不同的世界彼此相遇」

阿魯斯的雙手手肘靠在桌子上，手掌則是在交錯握著放在下巴附近，這是西法卡常做的動作。

「如果就這樣放著不管，這邊的世界與那邊的世界會互相衝突並彼此消滅，解決這個問題的對策有好幾種，而我等選擇使用其中一種」

阿魯斯接著長長的嘆了一口氣，一副不太願意說出口的樣子。

「我等將要毀滅地球」

───

這句話要確實傳達到每個人的腦袋中需要數分鐘的時間，

阿魯斯耐心的等著這段空白的時間，差不多經過３分鐘後，他還沒繼續說下去前，地球就已經陷入一陣混亂當中了。
這是理所當然的，突然出現的人物宣言著世界會毀滅，
以美國為首的世界強權在那一瞬間都下達了備戰命令，

「另外，我等有打算接受來自地球的移民，只是移民數量有上限，差不多就是６億人左右」

魔族領的官員們拚命計算的結果是最多可以接受１０億人，但說實話沒必要接收到上限，畢竟人口數本來就會持續增加，為了保障生活能維持一定的水準，人數少一點會比較好。

「我們會接受科學家以及那些擁有特殊技術的人們以及他們的親人，我們不會接受那些沒有足夠生產能力的人」

魔王態度冷淡的持續說著。

「另外病人與老人除了擁有特殊技術者以外，都一律不允許移民」

可以用著冷酷的態度說著『我們不需要會扯後腿的人』，這也是要成為魔王的條件之一吧。

「在這之後給你們５天的時間考慮，５天以後在日本時間正午時刻，我們這邊會派遣數名使者前往日本的國會大廳」

這樣說完後，阿魯斯悠閒的坐了下來。

「另外就是想要入侵這邊的世界是⋯不可能的。你們可以儘量嚐試沒關係，但我們的設定是會排除所有生命體的。至於一旦這邊受到攻擊的話，會直接將之視為宣戰布告，並會盡全力將開戰的國家給消滅」

阿魯斯表達出『想試看看的話儘管來』的這種態度，不過他心中已經預設了某幾個可能會魯莽發動攻擊的國家。

「那麼就期待在５天之後在日本與各國首領進行會談了」

話一說完影像就消失了。

───

「唉～果然向整個地球公開說話讓人非常緊張阿，我應該沒有咬到舌頭吧？」

阿魯斯把面具拿下來後，整個額頭充滿著汗水，他拉著衣領啪啪的搧著風。
他將各國王族都招待來到一個類似大學教室的地方，不過也有許多人因為距離的關係無法前來，這類情況則是用通信裝置轉達。

「說起來那個內容還真是非常武斷阿，難道地球上沒有類似神竜的存在嗎？」

如此詢問著地球戰力的人是雷姆多利亞國的西昂王子，他是戰術天才，並在之前的戰鬥中對抗過擁有壓倒性戰力的魔族。
阿魯斯有禮貌的接下了別人遞過來的水杯，一口氣喝光並喘了口氣之後才開口回答著西昂王子的疑問。

「這部分沒問題的，雖然那邊有著能讓這邊化為一片死寂世界的武器，但是沒有能夠用來摧毀行星的力量⋯換句話說就是將整個世界徹底毀滅的力量」
「但就算如此也不能小看呢，這樣就需要相當程度的結界才行。不過您只提到了武器這一部分，至於地球的魔法師，他們的強度大概有多強呢？」
「欸？」
「既然武器都有這麼強的力量了，那麼那個世界的魔法師應該也擁有相當程度的戰力吧？」
「阿、阿～」

阿魯斯忍不住用手摀住自己的額頭，為自己的愚蠢進行反省。

「這個嘛，地球上是沒有魔法的，至少就我所知的情況下，完全沒有人懂得施展魔法」
「阿？」

這次是西昂驚訝的張著嘴巴，畢竟這個世界是以擁有魔法為前提而發展起來的，所以無法理解也是很正常的。
通訊手段、農耕、畜牧都利用著魔法，所有魔道具都是以魔力形成的塊狀魔結晶作為燃料，而最先進的科技則是使用著魔核。

「也就是說，那是個靠魔結晶來維持文明的世界嗎？
但是要製作魔道具的話還是得需要魔法吧⋯」
「這個嘛，讓我稍微解釋一下地球的文明好了」

他接著開始詳細的從石器時代、古典時代、中世紀、文藝復興時代、近現代一路說明到現代的科學文明發展過程，特別是燃料這一部分的變遷僅他所能的詳細解釋了一番。
而針對蒸汽機所開啟的工業革命，同樣被召來現場的薩基反而更清楚。
畢竟魔王已經超過一千歳了，所以過往的事情反而記得不是很清楚。

「說到底就是⋯以數學為基礎的學問、技術非常優越，這一點難道不能在這個世界上活用嘛？」
「可以阿，但必須要很慎重進行。因為利用科學力量創造出機械後，便能取代大量人力，這樣會造成大量人口失業阿」

而這些失業人口會流入都市裡，然後造成治安開始惡化，這點在這個世界也是一樣的。
至於先前因為魔族侵略戰而產生的戰爭難民們，現在也一一開始踏上返鄉的旅程。
西昂王子就這樣若有所思的樣子，陷入了自己的世界裡。

───

眾人接著問起各式各樣的問題，而阿魯斯在靠著同樣是轉生者托爾的協助下一一回答這些問題。

眾人雖然也很驚訝地球上竟然沒有技能或天賦，但這也有可能只是沒有被數值量化但實際上卻存在著，例如無法被量化的鑑定技能本身並不需要使用魔力。

「原來如此，還真是個讓人越聽越糊塗的世界阿」

這句不知道是誰的嘀咕聲完全道中眾人的心聲，不管是一千年前的阿魯斯還是兩千年前的托爾，他們都有嚐試傳遞這些地球的技術，但卻完全沒有被流傳下來，成功流傳下來的只有魔族領而已。

「明明就是被召喚來此處的勇者的故鄉，卻完全沒有魔法還真是讓人覺得不可思議阿⋯」
「在歷史中的確有類似魔法的一些微妙痕跡，但現在這些完全都被當成是迷信了⋯」

關於這點阿魯斯自己也感到半信半疑，例如莉雅前世使用的氣功，也不能說不是一種魔法。

「總之先讓我們談一下今後的情況，毫無疑問地球的軍隊會嚐試攻過來吧，特別麻煩的大概是飛彈這一類武器吧」

關於這個困難點，阿魯斯對著眾人們簡單扼要的說明著。

「那是一個很巨大而且可以從竜骨大陸一端飛到另一端的武器，武器內最強是相當於核爆魔法的威力」
「核爆指的是火系魔法中的那個禁咒嗎？」
「是的」
「如果是這種程度的話⋯就算是巨大都市的防禦結界也不一定防的住喲」
「關於這種武器的應對方式魔族領已經準備好了，而神竜們中至少也獲得了奧瑪的協助」

會議室中喧騰了起來，不過對於守護世界的神竜來說，如果核武器將人類的居住空間給破壊掉的話，也會對他們帶來困擾吧。
巴魯斯專心於破壊地球這件事情上，拉娜因為將這一代的勇者送到了異世界而消耗掉不少力量，稍微有點疲憊的樣子。至於瑟魯的話，他會在對方入侵這個世界大氣層的瞬間使用其力量吧。

───

漫長的會議結束了，阿魯斯明明沒有很勞累卻一副精疲力竭的樣子，讓莉雅冷冷的瞪著他看。

「辛苦了」
「不會，因為有你幫忙的關係所以不算什麼，果然有念到大學的人就是不一樣阿」

開會與交流的工作都交給薩基了，莉雅在這方面幫不太上忙。

「話說回來⋯就算我們最後一定會贏，但也一定得付出犧牲吧」

莉雅非常在意這點，既然已經決定站在這個世界的立場上進行戰鬥，那麼就打算僅可能減少己方的損失。

「雖說是總體戰，但只要美國別狂轟濫炸的話，應該是不會出現太多損害」
「這個嘛，就得靠我跟歐捏醬去負責交涉了」

───

是的，在阿魯斯發出的５天期間裡，莉雅與薩基會先行前往地球。

雖然也有邀請菈比琳斯一起去，但沒想到她躲回迷宮裡了。在她前世認識的人當中似乎沒有特別想見面的對象，不過身為精靈的她走在地球街上的話，一定會引人注目的吧。

「那個準備好了嗎？」
「沒問題，接下來馬上就要飛過去了」
「不過實際負責轉移魔法的是我就是了」

移動方式是靠薩基使用轉移魔法，雖然也可以用飛行魔法突破結界而去，但很可能會被地球那邊給觀測到吧。
轉移的地點首先是日本，至於要採取什麼樣的行動也已經決定好了。

「莉雅、薩基請小心注意喲」

這次必須留下來的卡拉一臉擔心的樣子，一邊親吻著莉雅的臉頰一邊說著。

「不用擔心，真有萬一的話就變身成竜飛回來」

薩基不知道為什麼總覺得莉雅的話成真的可能性很高，但此時從一旁傳來喊叫聲。

「大公」
「這不是殿下嘛」

來的人是剛剛在會議中踴躍提問的西昂王子。

順帶一提，身為王子的西昂與身為大公的莉雅相比，是莉雅的身分比較低。因為大公不在是王族的關係。

「已經要出發了嗎」
「是阿，要走了」
「這樣阿，如果可以的話，希望你能帶一些地球的武器回來，尤其是那個核武器」

莉雅對於這個要求感到一愣，但很快就微笑著問著原因。

「雖然是對上我們沒有太大勝算的武器，但還是讓人想研究看看阿？」

地球出身的三個人彼此互看了一下後視線都朝向阿魯斯看去。

「殿下，那個兵器有著非常微妙的問題存在著」

至於說明就交給阿魯斯了，兩人再度轉頭看著卡拉，

「那麼我走囉」
「我會買一些禮物回來的」

雖然薩基用著輕浮的口吻說著，但他隨即便全神貫注了起來，

這跟平常使用的短距離轉移不同，因為是要前往地球這另一個世界，所以構築術式需要大量時間。
但薩基腦中卻想著不幫克莉絲也買點禮物不行這種輕鬆的事情，就這樣術式構築完成，兩個人一起轉移了。