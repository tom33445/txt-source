天空的顏色毫無生氣。

和我至今為止所見的天空都截然不同。雖然是夕陽那般的橘黃色，但無論是高空還是地平線附近，不管哪裡的顏色都完全一樣，仿佛像塗上了油漆的鐵板一般，觸感似乎很硬的天空。

奇怪的不僅僅是天空。天空下蔓延開來的街道也與我所知道的街道相差甚遠。根部有著像海膽一般的刺，一部分樓層還很誇張地凸出來⋯⋯ 我想那大概是大廈吧⋯⋯ 雖然由我這個前段時間期末考試物理分數如同婚齡一般的人來說不太好（注：日本人男的婚齡是１８ 女的是１６），但是那個物體這樣都不倒就只能認為是賄賂了物理法則之神啊。旁邊那個染上了青系的熒光色的建築物還是倒三角形的，再過來的那個建築物甚至看上去像是漂浮在空中一般。每個都非常地大根本無法判斷距離有多遠。

「這到底是⋯⋯什麼啊？」

這些話不由地從口中冒出。與那些瞎眼的風景相反，周圍卻非常安靜。沒有人出沒也沒有鳥與虫的氣息。這裡到底是哪。我從來不曾想過要到這種世界來也不知道來這裡的方法。真的只是在無意間就出現在眼前這片景色裡，我如此地煩惱著。

忽然，肩上似乎有什麼東西在蠕動。突然來到未知的世界裡，肩上還有迷之生命體，這樣的狀況下本應該抓狂才對的，但我對於這個觸感卻有所熟悉。

「小氦」

我所寵愛的寵物，扁鍬形虫小氦。

逍遙自在的它完全看不出是節肢動物那般地喜歡粘著我，即使拿去放養也不肯離開我。在它的胸部綁上粉色的絲帶也不見得有所厭惡。飼養了十年鍬形虫的我也從來沒見過這麼機靈順從的可愛鍬形虫。啊啊，好可愛。明天要換什麼樣的絲帶好呢！

不過現在不是被小氦的可愛之處所感動的時候。這裡到底是哪裡？好不科學，卻很宏大，簡直像是在漫畫裡所看到的未來世界那般的誇張。我和小氦本應處在那平凡的，天空是藍色的，大廈是遵守物理法則的，有著二級河川的，還有便利店和會在各站停車的車輛，總之平時那個由平凡要素所構成的世界一瞬間變成老家那樣了嗎。我們已經想回去了。

話是這樣說，可一直傻站在著就算能回去也回不去了。要是錯過了小氦的晚餐時間的話，我作為飼主可就失職了。雖然有點小害怕，可也得去調查一下這裡是哪⋯⋯！

總之先冷靜地從周圍開始觀察。我現在所處的小道上，是由威嚇昆虫而進化的像翅膀一般色調的磚塊所舖成的。周圍沒人，至今為止一台車也沒有路過。信號燈和十字路口也找不到，與其說是公路感覺更像在公園裡面。竟然還有如此喜歡擁擠的自治政府。

走了一段路之後在左邊發現了河流。看來這條路似乎是沿著小河的散步道。再走了一段路之後散步道上的橋開始交錯。⋯⋯是普通的橋。於此之前所看到的東西不同，是我見過的那種顏色的混凝土和鋼筋所制成的。連天空的顏色都不正常的這個世界，似乎並不是所有的一切都與我所處在的世界有著不同。有一部分看上去還是很正常的。

（啪！）

一股勁地往長椅上坐下那一瞬間，原本安靜的周圍響起了違和的聲音。要是有漫畫裡從鼻子將意大利麵噴出來的情節，再伴隨著調好的草藥那種異樣的漏氣一般的擬音。當我放棄對那個無法令人放鬆警惕的聲音有所警惕的時候，長椅後面似乎有什麼東西在動。我就連慌忙回頭的時間都沒有，那東西伴隨著齒輪滾動的高音以驚人的氣勢繞過了長椅，一瞬間就到了我眼前。那並不是生物，而是某種機器。

「什麼！？什麼什麼什麼！？」

我立馬就後悔了。聽到那個聲音的時候就應該逃跑才對。不，在這之前就不應該悠閑地往長椅上坐。因為那個機器看上去對人體一點都不友善。金屬桶制的身體上奇怪的尖端部分卻有著好幾隻手。似乎能將好幾十人分的心臟一次擊穿的尖細的刺在無數地增加，看上去能將內臟從嘴裡強行吸出來的橡膠杯，這個機器看來能一次性享受多種拷問模式。而最恐怖的部分是左右各拿著一根帶著很大的口子的圓筒正向著我襲來。那個口子伴隨著激烈的聲音將空氣吸入其中。一定是有著超群的吸引力吧。那個機器是射門人，我是守門員，我們開始了如此般的對視。雖然沒有發動攻擊，但是卻在漸漸地接近中。

雖然不清楚是因為什麼原因刺激到了這個殺戮機器（？）。小氦也在肩上顫抖著。唯獨小氦的生命我一定要守護！一定要在這個漸漸逼近的殺戮機器（暫）忍受不住殺戮之前！

「你 你好⋯⋯！」

首先要進行溝通。它很可能是這個世界？這個星球？的原住民？所以先熱情地打個招呼。我們完全沒有敵意，如果有哪裡失禮了也能方便下跪求饒。總之尋找對話的！傳達吧，人的心！

⋯⋯⋯⋯⋯⋯

沒有什麼特別的反應。嗯，多少我也察覺到了，那東西，要和它對話是不可能的。並不是語言上有差異，而是這傢伙本身就並不像是生命體。毫無感情，冷酷無情地吸引著東西並向著我這邊移動⋯⋯ 我的背後是長椅。要從張著左右手襲來的它的側面逃跑很難。而且從剛才繞過長椅的動作來看，逃跑這個想法本身就不太可能。這傢伙到底搞什麼啊⋯⋯ 我到底做了什麼啊⋯⋯ 我漸漸地有點煩了！

「你這個呼吸過度」

不懂。我不懂「呼吸過度」作為壞話是否成立。反正對方似乎聽不懂我說話，能怎麼說就怎麼說。我這麼想的時候⋯⋯

（沙吱⋯⋯沙吱）

好像在說什麼！不，是響了起來？從殺戮機器深處響起了用來調製色拉感覺會很好吃的危險的聲音。是因為我那個連壞話都算不上的壞話而有所反應了嗎。到底是什麼和什麼摩擦才能發出那種聲音啊。雖然搞不懂的事有一大堆，總之感覺是有點生氣了。小氦，抱歉，我快不行了。腦子已經跟不上了。突然到了一個奇怪的世界，被奇怪的機器襲擊，還有好多奇怪的聲音，到底怎麼回事嘛！

當我向左肩上的小氦投向放棄的視線時。

（夾）

「好痛！」

突然小氦夾了一下我的鼻子。什麼？喝斥嗎？雖說是夾，但也只是類似狗向主人撒嬌時那樣輕輕地夾一下。沒有惡意的那種。

「⋯⋯要我別放棄嗎？」

嗯，不行哦。小氦如此說道。沒錯，不能這麼放棄。雖然不清楚這個狀況是在做夢還是現實，總之不回去的話就沒法讓小氦遲到果凍和西瓜了！

首先先想辦法搞定這個殺戮機器（笑）吧。現在這個僵持的狀態不會持續太久。既然如此，先下手為強！語言不行的話就用肉體語言來決勝負吧！

有個招數叫做Drop-Kick（其實是指落地的球未反彈就踢出去）。是一種向著對手進行助跑跳躍，雙腳豪爽地進行飛踢的技能。因為要很強勢地進行猛烈的跳躍，所以像我這樣的女生或小孩只要擊中了就能發揮很大的威力。我的運動神經並不好，我這種體育成績無論是否努力，５個等級只能拿到２的女生，憑著完全不考慮落地的事就跳出去的精神狀態。感覺我跳得非常高。

（咚──）

成功了！在伸向前方的吸氣口的上面一點，我將雙腳深深地踢向了它的胸口。它發出了很大的一聲之後就向後倒下了。不要怪我！不，應該說能受到女子高中生毫無修飾的Drop-Kick你該感到高興才對。

趁著殺戮機器（原）還在蠢蠢欲動的時候，我逃向了剛才那座橋的橋下。真是辛苦的一戰⋯⋯ 最後還是沒搞懂那個到底是什麼機器，有那種東西在這周圍徘徊著，這下根本就無法隨意調查這附近了呀。真是四面楚歌。

我在橋的內側往上看。乏味的混凝土蓋住了我的頭頂，看不見那片有奇怪顏色的天空倒是讓我平靜了一些。橋下的風景早已司空見慣。從以前開始一旦發生了什麼討厭的事情令緒低落的時候，就會有到附近的橋下雙手抱膝坐著的習慣，直到剛才我也是到平時那座橋下⋯⋯

對了。

就在那座橋下。

那是我到這個奇怪的世界之前最後的記憶

之後為什麼會變成這樣了。冷靜點回想一下。回想今天。還有我來到這個世界的契機或者原因，我想一定會在其中

───

我叫淡路奈津美、１９９９年７月３日生。現在是２０１６年７月２１日，而我在不久前才成為１７歲。在一間非常普通的公立高中就讀，學習成績是每當考試時期才命努力逼近平均分的程度，而運動神經則是賭上了決死的覺悟才學會了Drop-Kick的水平。沒有什麼特技，也沒有加入什麼社團。唯一的興趣就是飼養鍬形虫。

我有兩個煩惱。一個是牙齒不整齊（畸齒）。這是個很令我苦惱的問題，平日中都盡量不露出自己的牙齒。在很久以前就因此造成了心理陰影，而如今，班上的同學日漸變得漂亮起來，我更是感到不好受。

還有一個⋯⋯是我從懂事以來就一直戰鬥至今的煩惱。但是，我卻不曾像今天這麼煩惱過。

明明在冬天裡偷懶了那麼久的太陽，這個月卻像個剛結束了長假的學生一樣精力旺盛。明明很快又會變冷的，一大早窗簾的間隙卻射入了強烈的陽光，而我這個有著嚴重遲到癖的人則因此準時起床了。好不爽。

今天就算遲到了也無所謂。期末考試已結束，今天是結業式的前一天，要舉行班級球技大會，從以前開始就非常討厭競技的我現在非常地郁悶。又可怕，我又拿不到球，也投不出去，快樂的瞬間從來沒有過。

因此我就發動了超群的睡過頭手法等到同學們都燃盡的時候再去學校。故意去偷懶多少會有點害怕，於是就想加入些意外因素作為理由。今天上學的途中，給老奶奶搬東西之類的，幫助在去女兒的婚禮途中迷路的老爺爺之類的，我一邊尋找著這樣的人一邊走著。

把運動用品放入包包裡上學的準備就算完成了。剩下就是小氦的虫籠。放在家裡媽媽會因為噁心而不幫我照看它，所以我不管去哪都盡可能帶著它一起去。朋友們也完全不能理解我的嗜好。鍬形虫明明這麼可愛，卻不見有人飼養它們，甚至還有人會害怕它們，所以我進入高中之後就一直隱瞞著這件事。

「走吧，小氦。」

我將手伸入小氦住的舒適小籠裡，它很溫順地就爬到了我的手上。移居到包裡的小籠內的這期間沒有任何的抵抗，今天我也為它的乖巧而感到高興。這孩子只要為了我，就算是基因上會排斥的事情也會去做。我很愛它。

往外跨出一步就開始感覺到了熱量。因為出生在這個地軸帶有傾斜缺陷的星球，我們人類在炎熱和寒冷中如同彈球一般被擺弄著。因為我的名字裡有「なつ」的關係，所以我對「夏」這個字有著很高的仇恨值。不過我看到包裡的小氦似乎很高興的樣子。這點上只能說是種族差異了。

「⋯⋯今天就不散步直接回去好了。」

在適宜的天氣放學時都會照慣例讓小氦出來舒展一下翅膀，不過今天就有點⋯誒？

（嘎）

⋯⋯放入虫籠內的小木片被小氦緊緊地夾著。很明顯是反對的信號。這樣啊，我會妥善處理的。

在這絲毫感受不到清爽早晨，在完全不打算翹班的太陽下，既沒有因為重物太多而苦惱的老奶奶，也沒有在去女兒的婚禮途中迷路的老爺爺，我準時到校了。

「奈津美，早上好！」

進入教室時會向我打招呼的是同班同學由加莉。不知是因為今天要運動還是單純因為太熱，她將淡茶色的長髮綁成了馬尾辮。是個非常愛笑的女生，肯定會有很多男高中生會希望將手指伸入她那顯眼的酒窩裡吧。最重要的是她在笑的時候牙齒很整齊。

「早，早上好，哇！」

我草草地回應了她，不知是誰無意中放在地板上的包差點絆倒了我，我為了不摔倒向前走了幾步抓住了某樣東西⋯⋯

（沙沙）

原來是窗簾。抓住窗簾使衝擊減弱了不少，但是我那毫無防備的臉則貼上了地板。平沙落雁的我還抓著窗簾，但是這個窗簾下面並不長。窗戶的框架與我的腰部等高。原本窗簾的長度也應該如此才對。

也就是說，窗簾被我拖了下來。

嘈雜的教室中，兩個男生的聲音蓋過了在附近擔心我的由加莉的聲音。

「今天的第一次呢，淡路！」
「一大清早狀態就這麼好啊。」

我一邊擦著鼻子一邊站起來看著聲音的主人，第一句是一個叫悠介的人說的。長得很高的足球部部員，經常喜歡搞點鬧劇出來。我都懷疑那茶色短髮裡面是不是真的有腦漿。說第二句的人是雅司。這個人是網球部的，不僅是運動了得，學習也不錯。真是可惡。

「你們真是，都說過不要再數了！」

我像往常一樣反抗。

「要怪就怪你，總是做這種讓人忍不住去計算次數的事。」

悠介如往常一樣回應了我。

我另一個煩惱就是這個。

絕望般的遲鈍（天然）

不僅僅是遲到。連平地摔，善忘等任何廢柴要素都集中在我這個稀世鈍感人才身上，班上的同學都知道我一天至少有兩次會幹出點什麼來。像悠介和雅司這種喜歡計算次數來取笑我的人也有不少。順帶一提我至今的最高記錄為六次。

「沒事吧？沒有流鼻血吧？只要沒受傷就好」

不管我再怎麼失敗由加莉都會溫柔地安慰我。而且牙齒很整齊。

「嗯，我沒事⋯⋯」

我偷偷看向在一旁嘻嘻笑的兩個男生。

「希望今天不要再這樣了⋯⋯」

他們笑得更厲害了，一臉「那根本不可能」的表情。

「如果又有什麼失敗了就記得通知我哦。」
「煩死了！不要小看我！」

悠介不知是不是為了之後的球技大會才換上了運動衫，似乎很是興奮地在戲弄我。非常令人火大。今天一定要洗清污名⋯⋯

「不過，奈津美今天沒有遲到呢，運動衫好像也帶來了。」

由加莉的超級支援時間開始了，真是個好人。再加上牙齒那麼地整齊。我要是有兒子的話一定讓他取這種女孩。

「球技大會的日子怎麼可能會忘記帶⋯⋯不，奈津美的話的確有可能。表演大會忘記帶衣服，在龍宮內的魚群中，混雜著一個穿著體操服的人在跳舞，這可是在兒時玩伴間流傳的傳說啊」
「不要再提那個了！」

看上去雖然很文靜的雅司，但絕不能對他大意。我和他的家族之間從幼兒園開始就交往至今，我所干下的壯舉他基本都了解。不少情報都是這傢伙泄漏的悠介的。

「不過啊，球技大會中可別太遲鈍了。我可不想輸！今天可是鼓足了勁兒的！」
「不過躲避球應該不會有什麼問題吧。奈津美的話肯定會早早就到外野去的」
「說來也是！那麼，我很期待你的活躍哦！淡路！」

真是想到什麼說什麼。這種人無視就好了。由加莉握著我的手稍微遠離了他們。

「別在意，奈津美。今天不上課，所以不會像平時那樣忘帶課本和作業的，放心吧。」
「嗯，謝謝⋯⋯」
「不過那兩個人嘴上是那樣說，還是挺溫柔的。你看，他們在修窗簾呢。」

回頭望去，悠介脫了鞋子正站在窗戶附近的桌子上，利用他的身高將窗簾附帶的零件裝了回去。

「⋯⋯才不是的，那才不是什麼溫柔呢，肯定是為了看第二次才特意重新擺好的陷阱」
「怎，怎麼會嘛！雖然他們經常嘲笑你啦，不過他們不會因為你的遲鈍而生氣呀。一直都是笑笑就過了」
「這的確是的⋯⋯」

他們確實沒有責怪過我。但這是因為我幾乎沒有給他們帶來過困擾。這是唯一令我自豪的事，給班上的同學帶來麻煩的蠢事我從來沒有幹過。使教室的滅火設備發動，或者是讓滅火器突然爆發，亦或者是惹怒老師使自己的作業量增加之類的。以及我每日都會出現的善忘和遲到，會困擾的都是我自己而已。所以他們才能這麼高興地戲弄我。對他們來說我是逢時的玩具，就像一隻會奏出歡快音樂的猴子人偶一樣。

「我們也差不多該換運動衫了。你應該有好好帶來吧？」
「你不都知道我帶了嘛！連由加莉都說這種話⋯⋯」
「呵呵，抱歉抱歉」

我們一邊如此交談一邊從包裡拿出運動衫，此時我注意到了。

「啊！」
「怎麼了？」
「⋯⋯忘了帶便當了」

後方傳來了兩名男子高中生的笑聲。

這也是最後一天被這兩人得意地當猴耍了。

───

球技大會終於開始了。我們Ｅ班首先要進行Ｅ、Ｆ、Ｇ、Ｈ四個班的循環賽，以聯賽的形式進行戰鬥。得分最高的兩個隊伍進入決勝淘汰賽，流程就是這樣。

規則極其無情。首先，選手拿著名為球的凶器向著被稱為敵方區域內野的地方看去，以個人的獨斷來決定敵方選手在愚蠢排行榜上的定位。卑鄙地從定位低到高來選擇，用球以無法閃避的速度瞄準那些迷茫的弱者。中彈的選手將被監禁於一個名為外野的隔離設施，再也無法回來。最終內野留下的選手越多的隊伍將會獲勝，作為代價，將會失去與人之間的信賴。比賽時間是十分鐘。

勝負的關鍵是如何利用外野那些被幹掉的人。即使是剛才在內野與其有深刻友誼的人也不能對他們抱有任何感情。他們作為無論怎麼傷害也不會死的僵屍，被內野所使役是他們的命運。名為外野的設施是環繞著敵方區域的內野的，能夠非常有效率地追逼目標。居住在內野的特權階級持有者們只需要將球交給僵屍們就能得到好處。但是戰後會受到稱讚的並不是僵屍而是他們。只能說這個社會構造實在是太扭曲了。在教育設施內舉行這種遊戲真的沒問題嗎。

每個班的人數大約在三十左右，但是出賽者規定只能有二十人。選出這二十個人的方法很自由。但是每個人都規定必須出場兩場比賽。不能一直加入那不能出賽的十人團體。

但是我也不傻。從決定要打躲避球開始我就非常清楚這些，所以早就準備好不給班裡帶來麻煩的對策。

首先，作為大前提，我絕不會動球。因為我投的球能砸中敵人的機率幾乎是零。由二十人組成的躲避球隊伍對個人的責任要求並不高。早早地中彈或者是扔幾個奇怪的球並不會對勝負有多大影響。只不過，投球的失誤會很顯眼，唯獨這個無論如何都要避免。

再說躲避球在投球之前必須要接到球。我當然沒有接球的能力，不去投球這一點完全能無意識地做到，但是現實卻不是如此。

比如說，一開始就中彈進入人很少的外野將必須受內野指使去幹活。這樣就可能演變成必須投球的情況。可是到結束為止還留在內野也是不行的。因為這個情況下我去投球的機率也會變高。因此，我的作戰就是這樣，初期要盡可能地閃開，中期故意去中彈，後期就能在外野休息了。這個作戰無論在腦內模擬多少次都非常地完美。

實驗這個作戰的第一場比賽，對戰Ｆ班馬上就要開始了。

「我們同一隊哦，加油吧！」

我的天使──由加莉換上運動衫之後真是楚楚可人。接下來居然要用球對著這樣的女孩射，這個高中，這個國家，這個世界從根本上完全腐敗了。不，敵方隊伍的男生們估計不會對她投球。

這就是說，只要待在她身邊就不會有球飛過來！這真是好主意！初期就待在由加莉附近，想到外野去的時候只要離開她就好了！對面的男生面對我這種又弱，牙齒又不整齊的人肯定不會留情的。

「啊，悠介君和雅司君也是同一個隊伍呢！真是可靠！」
「那當然！搞起！我要開搞咯！」

聽到由加莉香甜的聲音，悠介的反應非常過度。好煩。

「悠介，你太吵了」

雅司似乎也有同樣的感覺。

「喂喂，雅司，你再拿出點幹勁來啊！我可是非常期待這個活動的，期待得連期末考試都慘不忍睹啊！不拿到優勝的話就沒法跟家長找藉口了！」
「只是因為你沒有學習而已吧。我也很期待這個活動啊，不過考試結果你懂的。」
「班級第四來著？是是，你的腦子很好總行了吧。這種事只是因為天生的大腦構造不同，只能認了。對吧？淡路」
「為什麼這裡要提到我的名字啊！」

悠介和雅司一副「這是必須的」的表情。我決定今後要是有機會，一定要讓小氦狠狠地夾他們的腳脖子或者膝蓋背面。

「好啦好啦。考試已經結束了，好好享受球技大會吧！」

被期末考試第一而且牙齒很整齊的由加莉安慰之後，悠介和雅司二人組馬上靜了下來。你們鬧哪樣！總是由加莉！果然

是牙齒整齊的關係嗎！？

悠介真的是幹勁十足，無論是什麼樣的球都能接得到，面對男生自然是不用說的強度，而面對女生則意外地細心投球，他１個人就將對面６，７個人送到了外野。與在外野的雅司之間的配合也非常華麗，經常將對手玩弄得團團轉。不過，悠介這麼出色或許並不是因為他很優秀，說不定是因為己方和對方都沒有像他那麼幹勁十足的人。反正我是決不會承認的。

最後我既沒中彈也沒投過球。看來待在由加莉身邊的作戰起效果了。就連待在球場內觀看比賽的餘裕都有了。由加莉偶爾接到從外野傳來的球也給隊伍的勝利做了貢獻。她的運動神經也很好。萬一被人問起小氦和她之間我更喜歡誰的話，可能會煩惱個五周。

因為悠介偶然的活躍，戰勝了Ｆ班的Ｅ班在下次對戰將會遇上戰勝Ｈ班的Ｇ班。不過我和由加莉，以及悠介和雅司在這次對戰將會休息。期間等了很久。

───

「時間雖然有點早，還是開始吃飯吧！咦？你沒帶便當來對吧？」

一臉傻笑向我搭話的人就是沒有實力偶爾活躍了一下的熟識，悠介。以及在一旁用一副清爽的表情發揮他腦內細胞進行煽風點火的雅司。

「抱歉，不可能！」

反正肯定想在吃飯的時候看看我會幹出什麼事。趕緊離這些傢伙遠點。如此想著的我挽著由加莉的手離開了他們兩人。由加莉的頭腦就如同她那美麗的牙齒一般完美，聰明的她似乎馬上就察覺到我的想法，什麼都沒說就跟過來了。身後傳來了兩頭雄性白痴的叫聲。很遺憾我那如同牙齒的排列一般糟糕的大腦無法理解它們的語言。

「奈津美挺受歡迎的嘛」

在我把買來的炒麵麵包吞下去的時候，從由加莉那有教養的口中說出了意想不到的話語。這是怎麼回事。難道是我在附近把傻氣傳染給她了嗎。

「受歡迎⋯誰受誰歡迎？」
「奈津美。受那兩人歡迎啊」

很莫名其妙哦。由加莉。

「那兩人，一直很在意奈津美呢」
「那，那是因為！他們在耍我啊！只是計算我失誤的次數⋯⋯尋開心罷了」
「是這樣嗎⋯⋯」
「就是這樣」

不，其實。我覺得他們只是想利用我來接近由加莉而已。雖然他們完全不把我當女孩子看，但是面對由加莉卻非常有禮貌。

他們估計很羨慕我能和由加莉一起在中庭的陰涼處吃午飯吧、

「那你覺得他們兩人誰更好？」
「誒！？哪個我都不要！」
「一定要選一個呢？」

由加莉感覺樂在其中。對別的話題多少帶點壞心眼的由加莉雖然很可愛，可這個也壞了。但是，我無法背叛她一直盯著我的眼神。

「⋯⋯唔。無論哪個我都絕對不要⋯⋯但是不選一個就要死的話就悠介吧⋯⋯」
「為什麼？」
「並不是說悠介好，只是我和雅司從小就認識，一直像手足一樣，那種事情根本想像不出來。」
「哼哼哼，排除法？」
「那肯定啊！」
「不過我也覺得你會選悠介」
「什麼意思嘛！為什麼？」
「不為什麼，呵呵」

雖然不知道為什麼由加莉會提起這事，但由加莉看起來格外地開心。

我想換個話題就對由加莉說起了鍬形虫的魅力，但她看起來完全不感興趣。不知道和我擁有同樣興趣的人會不會出現呢。雄性扁鍬形虫的大顎明明那麼地性感而且無畏。

「喂！」

好不容易能輕鬆一下，卻從不遠處的大廳裡聽到悠介那吵鬧而且煩人的喊聲。

「前一場比賽已經開始了，差不多該過來了！」
「已經這個時候了？我們這就去！」

由加莉真是溫柔，居然會去理那個類人猿。的確我們差不多該過去了，可一想到是那傢伙叫我們過去就覺得很火大。

───

抵達體育館的時候比賽正好結束。看過成績表，知道了我們Ｅ班剛才輸給了Ｇ班因此一勝一負，剛才打敗的Ｆ班是三負，Ｇ班是三勝，Ｈ班和我們一樣是一勝一負。預選聯賽最後的比賽，Ｅ班和Ｆ班之中的勝者將成為二位進入決勝淘汰賽。也就是說接下來我出賽的這場輸掉的話，Ｅ班的球技大會就到此為止了。背水一戰啊。

悠介反而因為背水之戰在興奮著，雅司也是比剛才比賽的時候更加的認真。

「啊啊！對戰Ｇ班的時候我也好想出場啊！我要是出場說不定就贏了」
「還是全勝，Ｇ班真是強啊」

由加莉將溫暖的心化作話語傳達給了立馬開始得意的悠介。好想去抱她。

「⋯⋯與其說是強，不如說像悠介那種情緒高漲的人似乎比較多」

雅司冷靜的分析估計是正確的。由這所平凡的公立高中的學生隨機分配組成的班級之間，能力差並不是很大。差距應該在於你是以玩玩的心情去打還是以認真比賽的態度去戰鬥。

「Ｈ班雖然輸給了Ｇ班，可那場比賽很棒！真是強敵⋯⋯ 不過這樣正好！大家打倒他們吧！」

似乎是被他的熱情所打動，其他的同學也莫名地興奮了起來。輸掉就結束的狀況似乎都丟到了腦後。在這種少年漫畫般的氣氛中，失敗是絕對不可原諒的。不過只要像剛才比賽時那樣待在由加莉身邊就沒關係了吧。如此一大意⋯⋯

「喂，淡路！你沒睡著吧？比賽的時候你要撐著點啊！」

咚地一下被敲了頭。敏感地察覺氣氛的悠介開始戲弄我了。要是往常的我肯定會馬上進行反駁，可因為剛才由加莉說了點奇怪的話，多少有點不好意思。

「⋯⋯唔？怎麼了？不和平時那樣發火嗎？」
「你，你好煩！我回去了！」
「等下要開賽你就回去！？」

不能輸的比賽開始了。一開始的主動權由Ｅ班獲得。大概會和第一戰一樣由悠介在內野大展拳腳，雅司在外野進行傳球追擊吧。悠介單手拿著球以輕視的眼神看著對面每個人。馬上使用假動作擾亂對手，然後向著對方區域被逼入角落的集團投球並且還打中了一個人。

「好的！」

他非常興奮。漸漸地大家也被他的情緒所帶動，內野和外野都同樣表示「Nice」！場上如此沸騰了起來。

不過一開始不管怎麼投估計都能砸中吧。雖然外野多了２人但內野還是有１８人。被狹窄的場地所限制無法自由行動。

但是，容易擊中並不是對方隊伍才有，我們的隊伍也是一樣。而且初期人多的內野基本上不會讓投出的球飛出去，只要外野無法用拋物線傳球，內野禁用一投就會交換攻守立場。所以真正的勝負是在雙方人數減少到１０對１０的時候才開始。那個時候我希望能離開內野。

儘管如此正如我一開始擬定的計劃那樣，馬上進入外野就必須要在外野幹活。在目送能代替我在外野活躍的人之前，我必須要留在由加莉身邊。這麼想的時候。

「呀！」
「由加莉！？」

我這麼想道的時候由加莉居然被對方男生集中了肩部。還是力量不小的一擊。可惡，不可原諒。萬一砸中由加莉的臉的話該怎麼辦。想放棄今後的高中生活嗎。

「沒事吧？」
「嗯。很遺憾我們要離別了，奈津美。要好好躲開哦！」

由加莉如此快活地向我揮手說著，然後向外野走去了⋯⋯我的完美作戰計劃啊⋯⋯！

「淡路！快投！」

是悠介的聲音。集中由加莉的球就在我面前，悠介讓我撿起來投出去。

「不，不行不行！我投不了！」
「快點投啊！」

氣場已經出來了，那絕對要贏的氣場。本來這群傢伙的情緒就已經高漲得嚇人，而由加莉被攻擊的現在，男生們已經非常地憤怒了。這個氣氛裡要是有人說「人家不可能啦」這種毫無緊張感的話肯定會引起公憤。

既然如此就沒辦法了。渣渣只要盡力也能被原諒的。對，肯定沒問題。所以，用盡全力！

「嘿！」
「呀！」

打到人了。太好了！是女生的悲鳴。抱歉，不要怪我。

咦？

對方陣營沒有傳來了「被擊中了！」的反應。而且剛才的悲鳴似乎剛才也有聽過⋯⋯

「由加莉！」
「由加莉！」
「哇！由加莉大人的尊容！」

慢了一拍的人們都發出了包含由加莉的名字的悲鳴。我也明白了。我全力投出的球擊中了身在外野的由加莉的臉。

「由加莉！抱歉，沒事吧！？」

我下意識地就無視比賽跑到了外野。球也掉到了外面去，這也算是暫停比賽。

「⋯⋯沒，沒事，奈津美。別在意⋯⋯」
「真的很抱歉！保健室！快去保健室！」
「呵呵，太誇張啦。我沒事的，你先回去比賽吧。好嗎？」

由加莉摸著鼻子仁慈地對我這個加害者表示關心。那排牙齒還是依舊那麼漂亮。我的確沒有能將一個人送到保健室的力氣，回去比賽好了⋯⋯無論從哪邊的陣營都感覺到了敵意⋯⋯主要都是男生的。真心不想回去。這種內疚的感覺自從我粗心將小氦之前的一隻扁鍬形虫小鋁害死之後是第一次。

在這種氣氛中第一個說話的人是悠介。

「淡路，快回來！別在意，如果準心對了就是個好球！」

⋯⋯這可能是我第一次對悠介心懷感謝。稍微緩和了一下周圍的緊張氣氛。難得溫柔一次嘛。看著傻笑的悠介，腦內突然閃過由加莉說的話。

⋯⋯不，沒那回事。

那傢伙大概只是想盡快重新開始躲避球的白痴罷了。我在心中向由加莉，以及男同學們，有必要的話還要向全世界道歉。如此想著回到了場內。在遠處的球被雅司撿了起來重新開始了比賽。

今天第三次失敗了。而且還是給別人添麻煩的類型。好郁悶。接下來絕對不能再幹出什麼來。要盡早去向由加莉好好道個歉。必須趕緊被外野收監。

不知是不是Ｅ班的人將對我的敵意轉向對方，戰鬥變得很順利。不僅是悠介，其他男生都完全成為了熱血漢。但是從內野人數上看是１２對１３，我們這邊略微地弱勢。

時機已到。雖然有點早，但這也是迫不得已。必須盡早前往外野向由加莉下跪認罪。並且是額頭死貼著體育館地板的謝罪方式。正好籃球部的強壯男生去了外野，他會和雅司一起努力的。

特意被擊中並不是那麼難的事。只要行動緩慢一點就會馬上以我為目標。特意去磨磨蹭蹭偷懶是我的專長，同班同學也都應該看慣了我這樣，估計不會有違和感。

球傳到了對面去了，外野的人將球又傳了回去。右邊來球就往左邊閃，左邊來球就往右邊閃。在球快速地進行傳遞的時候，倘若有一人慢了一拍的話，球就會飛向那個人背後。我是如此深信著的。

「淡路！」

熟悉的白痴男聲音呼喊著我的名字。終於來了！看來我被盯上了！只要不進行抵抗⋯⋯

「噗哈！？」

⋯⋯球並沒有擊中。只是摔倒時發出了奇怪的聲音。似乎因為我的摔倒而華麗地躲過了背後襲來的球。頭上閃過了一陣風聲。搞錯啦，這裡才是停車站啦。與我所期待的相反，觀眾們都認為這是高端技巧為我鼓掌了。

「哈哈哈！淡路，今天的第四次了！」

接住了我所躲開的球的悠介理所當然地看出了我那並不是什麼高端技巧。瞄了一下外野，發現雅司也笑了，而由加莉則用手捂住了嘴。

「剛，剛才是！」
「你遲鈍起來居然還能派得上用場啊！哈哈哈！」

他很明顯看出了我是摔倒的，可我姑且嘗試了反駁。不過悠介一副沒有聽到的樣子用笑聲打斷了我的反駁。這人真是過分。

笑了一段時間後同學們都紛紛對悠介示意讓他開球，悠介「是是」地回應之後毫無做作地投出球解決了敵方的一名選手。不是的，我根本沒打算幫忙。我只想趕緊被擊中然後到外野進行三天兩夜的下跪旅行。

之後我依舊嘗試假裝躲慢了，但敵方的外野卻幾乎不理我。侮辱了由加莉大小姐的人就在這裡啊！看看我這糟糕的牙齒吧！就不想拿點什麼打過來修整它們一下嗎！？

如此來往了一段時間之後，我們的隊伍就剩下了７人。雖然有內野的悠介和外野的隊員們在奮鬥，但形勢還是很差。再不退場的話一定會遭受很悲慘的事情⋯⋯ 萬一成為了最後一個人就慘不忍睹了。當我準備自暴自棄地衝向中心線金雞獨立的時候，雅司傳球失誤交換了控球權。機會來了！

敵方在外野進行傳球的同時也在尋找著獵物。但他們十分慎重，在命中率沒有提高之前絕不會投球。這裡有只短腿的獵物啊。快看，是這裡。嘿嘿嘿。

球從外野傳到了內野選手的手裡，那個人看著我的方向用幾乎要說出「就是現在！」的勢頭心動了。隊友們都一邊看著球一邊進行著快速的閃躲。我只是緩緩地進行著後退。來吧，到我這。

「嘎！」

奇跡發生了。又搞砸了。這次是後腦衝向了地板。求從我的上面飛了過去。

球又被悠介接到了，那傢伙，笑得眼淚都流出來了。外野的反應比之前更厲害，雅司發出了爆笑聲，不經意地看到由加莉也在笑。怎麼會這樣⋯⋯

友方的爆笑，敵方的的騷動。投球的那個人還一副「不僅第一次連第二次也躲過了⋯⋯！這已經不能說是偶然了！」的表情看著我，而外野則是「那個女人⋯⋯ 好厲害！」這般的騷動。不是的。這是今天的第五次那啥啦。

不知道是不是因為我那罕見的行動的關係，Ｅ班的情緒突然急劇上升。剩下兩分的時候終於追成了２對２，然後雅司使用了被稱為「原外」的權利回到了內野，也就是一開始就一直在外野的選手，加上回到內野的雅司就是３對２。

已經完全接近尾聲了，作戰完美地失敗。剩下就只能期待自己不是最後一個留在內野的人了。但是因為使出了兩次高端技巧展現拔群的回避能力之後，我似乎誤打誤撞地使他們放棄了我這個目標。那是誤會啊。我真的會乖乖地中彈的。

「淡路！再努力一下吧！」
「這樣下去就能贏了！」

最後留下的白痴二人組異常地興奮。簡直就像青春電影。但青春是短暫的，熱情一瞬間就冷卻。還剩下一分比賽就結束了。

我不出意料地經歷了這場比賽的第三回，今天第四回跌倒的慘痛經歷。論失敗次數就是第六次，歷史以來最高紀錄的瞬間。

不過這次和前兩次不同，沒有躲開球。向前摔倒的我被球擊中了腳心⋯⋯ 被對手的強力投球所擊中⋯⋯ 而速度很快的球反彈了回來擊中了悠介的肩部⋯⋯ 再次進行反彈又擊中了雅司的腳關節⋯⋯ 然後靜靜地落在地板上。淡路彈射器完成了。

示意比賽結束的哨聲想起。規則上表明，在球落地之前擊中複數的人都算作是出局。突然的結束使Ｅ班全員仿佛像時間停止了一般。誰都笑不出來。

───

球場上的選手退場之後，混在沒有上場而是來助威的那１０人中，終於冒出了「真是可惜呀」「真遺憾」之類的話語。即便如此，悠介還是無動於衷。一副小孩子被人拿走了最喜歡的玩具的表情。同學們逐漸地走向了更衣室，只有我和悠介沒有移動半步。

「抱歉⋯⋯」

我能做的只有道歉。悠介比任何人都要認真，大家也因此非常地開心，好不容易能有個好結果了，然而卻因為我的問題失之東流。他一直都用笑容原諒失敗的我，現在卻因為我的失敗讓他失望了。

雅司和由加莉因為在意我們倆的情況走了回來，但悠介還是什麼都沒說。似乎不知道該說什麼。他並沒有遷怒於我，對我說「都怪你」這種衝動的怒喝。正如由加莉所說的那樣，悠介真的很溫柔。

「好啦，多虧了悠介比賽才那麼精彩嘛。這就足夠了吧？」
「是，是呀！大家都很高興呢，我也開心哦」

雅司和由加莉在一旁幫我說好話，這使我十分心疼。由加莉為什麼對我這麼好，明明把球砸到你臉上的人是我。對我這麼好反而讓我更難受。

「⋯⋯是啊」

多虧了兩人，悠介終於開口了。

「輸了就是輸了。不過區區個球技大會也沒關係啦！可是淡路，待會要請我吃冰淇淋哦」
「嗯，嗯！雅司和由加莉的份我也會請的！」

要我請多少冰淇淋都沒問題。我如此想道。

預選聯賽結束是在下午１點多。姑且決賽後還有一場表彰大會，不過這和輸了的人沒什麼關係。今天的出席記錄只算早上的，偷偷溜回去也沒關係。就在離開體育館前往更衣室的時候，我們察覺到這件事。先去超市買冰淇淋，之後還有很多時間。

「對了，我們待會去哪玩吧？」

提議人是由加莉。不知是為了幫助悠介釋放考試期間的積憤，亦或者只是自己想玩一下，雖然不知是那方面，但這一句話似乎使悠介的情緒突然高漲了起來。

「喔，好啊！找些什麼來玩吧！玩什麼好呢！」

淡路彈射器發動時的表情一瞬間明朗了起來。雅司也受其影響。

我⋯⋯說實話，今天一直在進行反省，完全沒有心情去玩⋯⋯

「對了！好想打保齡球啊！最近完全沒有去過」
「啊，這不錯」
「保齡球啊。我不怎麼在行，不過感覺挺好玩的。」

這裡有個和不怎麼在行都掛不上鈎的女人的說。我的投擲姿勢就和我的牙齒一樣糟糕。

「喂，淡路。今天去我想去的地方」
「嗚，嗚⋯⋯」
「這傢伙的水平連保齡球都怕她了。得分簡直和適婚齡一樣」
「果然是這樣嗎？那請務必讓我親眼見識一下！」

雅司透露了不必要的情報。下次還敢亂說些什麼的話我就把他小三時還尿床的事告訴由加莉，作為報復。

「奈津美。去吧」

受不住被由加莉用那種眼神看著。再說本來也是由加莉提出的，不能隨便地拒絕掉。

「⋯⋯嗯，走吧。先說好我真的很菜的！就連發明保齡球的人都想不到會有人能菜到這個程度」
「哈哈哈，真是期待！啊，不過先去吃冰淇淋吧！既然如此就趕緊的！」

這麼說道的悠介向大廳走去了。「喂喂」地喊著的雅司追了過去。呵呵地笑著的由加莉也跟了上去。

「等，等一下！」

跨出的雙腳。沒有任何負擔。地面不存在任何阻礙物。即便如此這雙腳無論多少次都能引發奇跡。刷新失敗的最高紀錄，歷史以來最大的失敗在此炸裂。

「噶！」

單說摔倒的話還是和往常一樣。但今天的SHOW現在才要開始。向前傾倒的雙手觸碰到放在大廳牆角的滅火器的黃色安全栓，然後拉開了。然後滅火器被我順勢拉倒。厚重的金屬與瀝青猛烈碰撞發出的沉悶聲響徹了整個大廳。

滅火器是有使用期限的。定期檢查想必會有，而這傢伙身外戶外，必定比其他滅火器保養得更多。

因此，誰都無法預料到。它會爆發由女高中生絆倒時順勢拉倒所引發的衝擊。

發出就像打開了震動過１００萬次的瓶裝可樂的聲音，滅火器在沒有主人使用下自主地將內容物射了出來。我一時不清楚發生了什麼情況，連悲鳴都發不出來就被白色的霧氣所包圍。

「發，發生什麼了喂！？」
「唔哦！？」
「奈津美！」

先行離開的三人各自對這個險惡的事態發出了不同的悲鳴並趕了回來。不僅僅是他們三人。偶爾路過這裡的學生們也是，發現事態後都發出了聲音。還聽到有人說要把老師叫來。

我離發射口有兩米遠，因此並沒有受到直擊。但滅火劑所造成的霧氣實在太濃，三人的樣子馬上就模糊了。總之我明白現在因為我的關係引發了一些不得了的事情，盡可能地往三人發出聲音的方向走去。

「淡路，沒事吧！全身都是白的！」

面對渾身是粉的我，三人都是同樣的反應，但悠介的聲音是最大的。

「我，我倒是沒事⋯⋯ 滅，滅火器！倒了！那個黃色的！掉了！啪地一下！」
「你在說什麼呢？」
「大概是奈津美摔倒的時候順勢把滅火器的安全栓給拔掉了吧」
「對對！」
「你，你居然聽得懂啊雅司！」

真不愧是來往時間最長的人，在我苦惱的時候都能幫得上忙。這樣的雅司也令我多少冷靜了一點。

「摔倒的時候手碰到了滅火器，然後就倒下來了！接著那東西就自己爆發了起來！」
「滅火器不把開關還是什麼打開之前不是不會發射的嗎？我以前在防災訓練裡學過」
「就是嘛！可它就是噴了！自發的！」
「可能是倒地時的衝擊使其爆發的吧」
「雖然不知道怎麼回事，總之這下很麻煩吧？那邊已經一片白了！老師都過來了呀！」

真的。有好幾個老師都來了。當然，他們都生氣了。

「這個是你幹的？」

向著這附近渾身粘著最多份的我說話的人是教物理的下田老師。偏偏是我期中考試考的最差的一科。自那之後我和下田老師的關係就不怎麼好，現在更嚴重了。

「可是老師，她不是故意的！淡路同學只是摔倒時不注意⋯⋯」

就連優等生由加莉都護著我。今天的內疚真不是一點點。

「是的⋯⋯ 摔倒時拉動了安全栓，然後就變成這樣了⋯⋯」
「唔。有這種巧合嗎」

我也覺得。被自己的才能嚇到了。

「詳細的事情之後再說。總之你們四人先把這裡打掃一下吧。」
「誒⋯⋯？」

四人⋯⋯ 這事是我幹的。我不想再給人添麻煩。這樣下去我會被內疚的心情壓垮的。

「老師，這都是我的錯。我一個人來做」
「你在說什麼呢。自己看看，這根本不是一個人能做得來的。反正這大概是你們幾個胡鬧所造成的吧。都給我去」
「可是老師⋯」
「別說了快去！」

可是老師，我要怎麼面對他們？我要怎麼樣向他們道歉？

「還好穿著運動衫」

由加莉這麼開口之前，大家都是一言不發。悠介的表情變成了比賽結束時的樣子，雅司則面無表情。而我則因為內心的歉意一直不知道如何開口。老師和看熱鬧的人離開之後只剩下我們四人，終於開始了交談。

「今天⋯⋯真的很對不住。我⋯⋯我總是給大家添麻煩⋯⋯」
「就是啊」

是悠介的聲音。低著頭的我抬起頭看向悠介。

「平時大家看到你失敗都只是一笑了之，可今天真是受夠了。總是會摔倒。你就不會學著點嗎？」

悠介用著少見的低語調說著，比平時那吵鬧的話語更加刺激我的心。

我也受到了意外的衝擊，的確如悠介所說的那樣，今天已經多次給人添麻煩了。會生氣也不奇怪，把立場換過來的話我早就生氣了。然而我的內心深處依然希望悠介會以笑容原諒我。

「好不容易能去玩一玩卻要在這裡搞這種麻煩的掃除⋯⋯剛才的比賽也是因為你才輸掉的！」

將想法化作了語言。

平時的話我都會說「不要數了」這樣還嘴，只有這種時候我卻自私地希望他會站在我這邊。已經不是說「今天第七回了」這種玩笑話的情況了

雅司和由加莉都在默默地進行掃除。他們已經找不到任何可以安慰的話語，都遇上了這種麻煩的事，估計都說不出什麼好話了吧。

我盡可能地利索幹活的同時，思考著明天之後該怎麼辦。想像著大家都覺得「那傢伙肯定又會幹出什麼事的別靠近她」如此躲著我，最後我將孤獨一人在一旁失落，這時眼中流出的東西被我假裝擦汗掩蓋掉了。他們一直以來都以玩笑和鼓勵支撐著我。摔倒了以笑容而對，幫忙修理被拉下來的窗簾。如今才發現這是多麼難得的事情。

掃除最後用了２小時，在我們打掃的大廳裡，迎來表彰儀式的Ｇ班正在滿臉笑容地走著。

───

「⋯⋯保齡球，還去嗎？」

四人已更換了校服站在更衣室門前。由加莉在沉重的氣氛中把這個提了出來。

「還是算了。我今天已經累了，回家吧」

悠介的情緒依舊很低落。

「這樣啊⋯⋯那暑假再說吧」
「⋯⋯嗯。回去了，雅司」
「哦」

兩人留下了含糊的答覆回家去了。雅司稍稍回過頭來向我示意「交給我吧」。即使他沒說出口，我也知道他應該和悠介一樣地氣憤，或許是因為交往時間比較長吧，對我引發的意外似乎有了抗性。不，或許是因為悠介已經忍無可忍了也說不定⋯⋯

「我們也回去吧？」
「⋯⋯抱歉，你先回去吧。我去告訴老師我們已經打掃完了⋯⋯ 順便讓他訓一下⋯⋯」
「我也一起吧」
「沒事沒事。讓我自己去吧。不能再給你添麻煩了。」
「⋯⋯這樣，那我先回去了」
「嗯。一路小心」
「呵呵，你也是哦」

由加莉微微一笑後就回去了。我竟然將球砸向這麼溫柔可愛的女生的臉，還因為一些毫不相關的事讓她幫忙打掃。我就和我的牙齒那麼糟糕。

被我毀滅的滅火器似乎是因為衝擊使頂端的零件錯位，才讓白色粉末狀的滅火劑從間隙裡漏了出來。過來回收的事務員大叔震驚地表示他第一次見到這種事。

───

這種引起了負面奇跡的日子只有一個地方想去。離上學的路稍遠的位置，附近的二級河川的河岸地，那條河的橋的橋下。我失落的時候就會去那裡和小氦一起玩，直到心情恢復為止。

那條河岸地也是我和小氦相遇的地方。這附近既非城市也算不上鄉下。在經由人工處理的河岸地內能於這麼棒的扁鍬形虫相遇是非常難得的。我相信那是充滿了緣分的命運般的邂逅。

我坐在橋下略濕的地面上把包裡的虫籠拿了出來，將小氦從虫籠裡放了出來。在我手上的小氦似乎因為被塞在虫籠內的關係略帶疲憊地舒展著翅膀。

「辛苦你了。抱歉，本來過來中午就能回去了，都是因為我的失敗⋯⋯」

時間是４點左右。處於非日非夜的時間點，此時的太陽也非常凶暴。小氦沒有發聲器官，因此沒有回答我，但相對的我會把它那份也說出來。

「今天我把記錄刷新了。大清早就把窗簾給拉了下來，還忘了帶便當，把球砸到了由加莉的臉上⋯⋯又摔了好幾次，輸了比賽，最後讓滅火器爆發了⋯⋯」

越是說下去我的情緒就越低落。竟然一口氣都說不完⋯⋯ 今天的我簡直就是引發奇跡的女人。負面意義的。

「悠介他非常生氣⋯⋯平時都會以笑容面對的，當自己也被卷入其中的時候果然還是會生氣呢」

小氦一副沒聽見的樣子在夾著我的手指。看來我的話沒有讓它聽明白，不過今天這個情況聽不懂或許更好。我用手指戳著它的背部以作回禮。

包裡的手機響了起來。是由加莉發來的短信。上面用著令人羨慕的漂亮圖畫文字，簡單來說上面寫的是「不要在意今天的事情」

「由加莉為什麼對我那麼好呢？」

我難道為由加莉做過什麼？今天都是把球砸到她臉上還為我說好話⋯⋯才學兼備性格又好，而且牙齒有那麼整齊。我這個一無是處而且牙齒又很糟糕的人羨慕她不只一點點。

但是今天由加莉搞錯了一件事。在中庭的長椅上說過，我很受歡迎的那件事，完全是她搞錯了。我和雅司的關係是類似兄妹一樣，我知道他初中時的初戀黑歷史，以及中學時他喜歡的女生，他也將我那些見不得人的失敗集保存在腦內。事到如今根本不可能發展成戀愛關係。

而悠介⋯⋯萬一就算他是喜歡我，不過今天也幻滅了吧。原因雖然是我引起的，可沒想到悠介會那麼生氣。可能是平時都在笑著，那種感覺反而更大了。我並沒有對悠介有那方面的意思，只是今天由加莉說了些奇怪的話才讓我意識到那方面去，自討沒趣，最後還被人訓斥，真是笨。

要是能回到今天早上重新來過的話，我一定不會把窗簾拉下來，記得帶便當，早早地退出比賽，決不接近滅火器。真是後悔。又冒失，又笨，有遲鈍，牙齒又不好⋯⋯

「好想將各種事重新來過啊⋯⋯」

如此小聲地說了一句，同時在夾著我的手指在玩耍的小氦背上輕輕地敲了一記手刀。

───

以上就是我到這個有著奇怪的漂浮建築的謎之世界之前所發生的一切。給了小氦一記手刀之後一抬頭我就發現自己被移動到了這個世界。就是說那記手刀是原因⋯⋯？

「快回去快回去快回去快回去快回去快回去快回去快回去快回去」

我盡可能地減少力度使用輕輕的手刀不讓小氦受傷，如果那個手刀是原因的話，再來一次肯定能回去的⋯⋯！

但是什麼事都沒有發生。到了這個世界依然待在橋下的我所發出的喊聲被瀝青路吸收後轉到了河面。不是因為手刀的關係嗎。可是其他原因我又想不到⋯⋯

「喂！你在幹什麼！」

突然想起了人的聲音。被嚇到的我轉向了聲音的方向。逆光中有人影。到這個世界後第一次見到人。而且語言也相同。

不過有點奇怪。這個人，真的是人嗎？頭非常大而且尖。因為逆光的關係無法看仔細，但至少輪廓和人類相差甚遠。我什麼話都說不出，只是張開口呆站在那裡。

「就是長著溺灣式牙齒的你！」

如此傳來了第二句話。

溺灣式牙齒？

是把我那鋸齒型的牙齒和山谷聯繫起來，用溺灣來形容我的牙齒嗎？是這樣嗎？是不是這樣？看來這是第二回展露Drop-Kick的時候了。

正當我想說「你小子啥意思啊！」這種不良女高中生的話的瞬間，看到接近我的那個人時，任何想法都消失了。

是頭髮。

將人類的輪廓完全扭曲掉的東西原來是頭髮。簡直就像在頭頂上插花，而且還不是古典的那種優雅的插花，是非常前衛，具有進攻性的插花。到了這個世界後看到了許多奇怪的東西，但這個人是最奇怪的。這種髮型在外面從來沒有見過，不管歷史經過怎樣的波折也不會認為這是一種時尚吧。

「我在問你，你在這幹什麼！」

男人用有力的強調向我搭話。之前只注意到頭部，近處一看之後我發現了。這個人是警察。雖然穿著和我所知道的警察有點不同，不過扣子上雕刻著我所知道的警察圖案。看著一聲不吭的我，警察開始說了起來。

「剛才這附近的清掃機器人發送了緊急信號。正在附近巡邏的我正準備去確認這件事，發現機器人倒在在那邊的長椅附近。幸運的是幾乎沒有任何損傷。你知道什麼嗎？」

⋯⋯知道很多。原來那個不是殺人機器而是清掃機器啊。看上去似乎裝備了大量的拷問器具原來都是清掃用具嗎？這就麻煩了。面對警察表示是我使用Drop-Kick了結它的話就糟了。

「不，不知道！」

這也太奇怪了。在橋下對著鍬形虫使用手刀的同時進行著尖叫的人還如此動搖的話，任誰都會懷疑的。

「⋯⋯是嗎。那麼你在這裡做什麼呢？」

看吧，果然被發現了。不知是否該把實話說出來。其實我是突然從其他世界裡過來的，然後估計我就被好心的警察帶到醫院去了。

而且這個警察，有點奇怪。髮型就不說了，從言行舉止來看應該是這個世界的居民。語言雖然相同，卻有著違和感。總覺得哪裡有著決定性的不同。不知是不是耐不住我一直像麻痺了一樣不出聲⋯⋯

「你在聽嗎？溺灣式牙齒的少女⋯⋯話說你穿的衣服真是古老啊」

又 說 出 來 了。

為什麼我從小就一直在意的心理陰影要被有著搞笑髮型的陌生大叔這麼表達！要用多遠的助跑才能用出使這個大叔昏迷的Drop-Kick呢。

「只是在對鍬形虫使用手刀而已！」

我怒了。但是很遺憾，內容卻不可思議。本來這手刀一開始就沒什麼意義。只是想和它玩玩罷了。於是順勢就變成了這樣。我發出了意義不明的大喊之後等待著警察的回應，抬頭一看發現對方比想像中更加地驚訝。誒？有什麼奇怪嗎？

「鍬形虫⋯⋯？那個是扁鍬形虫嗎？是十年前滅絕的鍬形虫啊！」

嗯？這插花在說什麼呢？

「十，十年前⋯⋯是２００６年？我和它相遇是在一年前哦」

扁鍬形虫這東西到山裡一抓就是一大堆啊，就連附近的河岸地都能見到呢。這個警察估計因為頭上的插花把智商都替掉了。聽了我和小氦之間的關係之後，警察更加驚訝了。

「你，你難道⋯⋯是從過去穿越來的嗎！⋯⋯用那個扁鍬形虫」

搞不懂這個人在說什麼，腦子沒問題嗎？

「過去？我搞不懂你在說⋯⋯」
「今年是２０６６年。據我所知，扁鍬形虫在十二年前被發現擁有穿越時空的能力，想利用那些能力的人進行了濫捕之後，十年前滅絕了」
「誒？⋯⋯哈？」
「利用扁鍬形虫進行穿越時空的技術並不是一般人能使用的。不過也有扁鍬形虫憑自己的意識將人類帶到其他時代的稀有例子。發現這個力量的契機也是某鍬形虫研究者突然進行了３００年的時間旅行而發現的。⋯⋯這些都是這個時代的人的必修課啊」

我跟不上了。

那個，也就是說。

「⋯⋯給鍬形虫一記手刀結果穿越時空了？」